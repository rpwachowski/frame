import Foundation
import AVFoundation

/// The PhotoSession object is an encapsulation of the components
/// necessary for capturing still-frame imagery. PhotoSessions
/// keep track of all devices, inputs, and configurations for
/// the session instance.
///
/// :Author: Ryan Wachowski
/// :Revision: 1.0
/// :Date: 03/20/15
class PhotoSession : AVSessionBase, AVSessioning {

    private let captureSession = AVCaptureSession()
    private var activeCameraInput : AVCaptureDeviceInput?
    
    override init() {
        if let ActiveCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo) {
            // Do something
        } else {
            // Do something else
        }
    }
    
    deinit {
        // TODO: Destructor stub in case of resource leak.
    }
    
/*- AVSessioning implementations -*/
    var ActiveCamera : AVCaptureDevice? {
        didSet {
            onCameraSwitch()
        }
    }
    
    func loadPreview() -> AVCaptureVideoPreviewLayer {
        var layer = AVCaptureVideoPreviewLayer(session: captureSession)
        return layer
    }
    
    func switchCamera() {
        for d in AVCaptureDevice.devices() {
            if let device = d as? AVCaptureDevice {
                let positions = (ActiveCamera!.position, device.position)
                switch positions {
                case (.Front, .Back):
                    fallthrough
                case (.Back, .Front):
                    ActiveCamera = device
                    break
                default:
                    continue
                }
            }
        }
    }
    
    func captureMediaData() -> NSData {
        var imageData : NSData?
        var captureOutput = AVCaptureStillImageOutput()
        captureSession.addOutput(captureOutput)
        var connection = connectionWithType(AVMediaTypeVideo, output: captureOutput)
        captureOutput.captureStillImageAsynchronouslyFromConnection(connection) {
            (imageDataSampleBuffer: CMSampleBufferRef?, error: NSError?) in
            imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
        }
        if (captureSession.running) { captureSession.stopRunning() }
        
        return imageData!
    }
/*--*/

/*- Class functionality -*/
    private func onCameraSwitch() {
        captureSession.beginConfiguration()
        if (!captureSession.inputs.isEmpty) { captureSession.removeInput(activeCameraInput) }
        activeCameraInput = AVCaptureDeviceInput(device: ActiveCamera, error: nil)
        captureSession.addInput(activeCameraInput)
        captureSession.commitConfiguration()
    }
    
    private func connectionWithType(mediaType: NSString, output: AVCaptureOutput) -> AVCaptureConnection {
        var connection : AVCaptureConnection?
        var found = false
        for c in output.connections {
            if let connection = c as? AVCaptureConnection {
                for p in connection.inputPorts {
                    if (p.mediaType == mediaType) {
                        found = true
                        break
                    }
                }
            }
            if found { break }
        }
        return connection!
    }
/*--*/
    
}