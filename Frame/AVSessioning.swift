import Foundation
import AVFoundation

/// Signature for Frame AV sessions.
///
/// :Author: Ryan Wachowski
/// :Revision: 1.0
/// :Date: 03/12/15
public protocol AVSessioning {
    
    /// Gets and sets the session's active device.
    var ActiveCamera : AVCaptureDevice? { get set }
    
    /// Generates the capture preview from the session's active device.
    ///
    /// :returns: Preview overlay
    func loadPreview() -> AVCaptureVideoPreviewLayer
    
    /// Switches between front and back activity.
    func switchCamera()
    
    /// Captures data from session input.
    ///
    /// :returns: Output data
    func captureMediaData() -> NSData
    
}