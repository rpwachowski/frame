//
//  SecondViewController.swift
//  Frame
//
//  Created by Ryan on 3/3/15.
//  Copyright (c) 2015 Ryan Wachowski. All rights reserved.
//
import AVFoundation
import UIKit

class CameraViewController: UIViewController {
    
    var cameraSession : AVSessioning?
    var processor : MediaDataProcessing?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        displayCameraAlert()
    }
    
    override func viewDidDisappear(animated: Bool) {
        cameraSession = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*-
    @IBAction func shutterButtonPress(sender: AnyObject) {
        var mediaData = cameraSession!.captureMediaData()
        processor = PhotoDataProcessor(sessionData: mediaData)
    }
    
    @IBAction func flipCameraButton(sender: AnyObject) {
        cameraSession!.switchCamera()
    }
    -*/
    
    private func displayCameraAlert() {
        var defaults : NSUserDefaults
        var alert = UIAlertController(title: "Frame would like to access your camera device.",
                                      message: "With access to the camera device, you can use our extended camera functionality to take great pictures!\n\n Select \"Not Now\" to dismiss this prompt. You may return to this screen at any time via the alerts button in the camera view.",
                                      preferredStyle: UIAlertControllerStyle.Alert)
        
        var accept = UIAlertAction(title: "Give Access", style: UIAlertActionStyle.Default) {
            (accept) -> Void in
            /* var response = Don't capture this yet. */ AVSessionBase.requestCameraAuthorization()
            return
        }
        alert.addAction(accept)
        
        var deny = UIAlertAction(title: "Not Now", style: UIAlertActionStyle.Default) {
            (deny) -> Void in
            // Fill in this closure
        }
        alert.addAction(deny)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    private func displayAlbumsAlert() {
        var defaults : NSUserDefaults
        var alert = UIAlertController(title: "Frame would like to access your photo albums.",
            message: "With access to your albums, you can edit and upload any picture!\n\n Select \"Not Now\" to dismiss this prompt. You may return to this screen at any time via the alerts button in the camera view.",
            preferredStyle: UIAlertControllerStyle.Alert)
        
        var accept = UIAlertAction(title: "Give Access", style: UIAlertActionStyle.Default) {
            (accept) -> Void in
            /* var response = Don't capture this yet. */ AVSessionBase.requestCameraAuthorization()
            return
        }
        alert.addAction(accept)
        
        var deny = UIAlertAction(title: "Not Now", style: UIAlertActionStyle.Default) {
            (deny) -> Void in
            // Fill in this closure
        }
        alert.addAction(deny)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

