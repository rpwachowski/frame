import AVFoundation
import Foundation

/// Poor man's abstract class
///
/// :Author: Ryan Wachowski
/// :Revision: 1.0
/// :Date: 03/23/15
class AVSessionBase {
    
    class func requestCameraAuthorization() -> Bool {
        let authorization = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        var authorized = false
        switch authorization {
        case AVAuthorizationStatus.Authorized:
            authorized = true
        case AVAuthorizationStatus.NotDetermined:
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo) {
                (request: Bool) in
                authorized = request
            }
        default:
            break
        }
        return authorized
    }
    
}