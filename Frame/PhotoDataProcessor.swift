import CoreGraphics
import CoreImage
import Foundation

/// The PhotoDataProcessor object is an encapsulation of the behaviors
/// necessary for processing raw data from still image AVSessioning
/// outputs and manipulating the resulting image.
///
/// :Author: Ryan Wachowski
/// :Revision: 1.0
/// :Date: 03/20/15
class PhotoDataProcessor : MediaDataProcessing {
    
    enum ColorOptions {
        case Brightness, Contrast, Saturation
    };
    
    private let originalData : NSData
    lazy var manipulationImage : CIImage = {
        [unowned self] in
        return CIImage(data: self.originalData)
    }()
    
    init(sessionData: NSData) {
        originalData = sessionData
    }
    
/*- MediaDataProcessing implementations -*/
    
    func modifyColor(magnitude: Float, option: ColorOptions, callback: (update: CIImage) -> Void) {
        var key : NSString
        switch option {
        case .Brightness:
            key = kCIInputBrightnessKey
        case .Contrast:
            key = kCIInputContrastKey
        case .Saturation:
            key = kCIInputSaturationKey
        }
        
        var context = CIContext(options: nil)
        var filter = CIFilter(name: "CIColorControls")
        filter.setValue(manipulationImage, forKey: kCIInputImageKey)
        filter.setValue(magnitude, forKey: key)
        manipulationImage = filter.outputImage
        callback(update: manipulationImage)
    }
    
    
    func reset(callback: (update: CIImage) -> Void) {
        manipulationImage = CIImage(data: originalData)
        callback(update: manipulationImage)
    }
    
}
/*--*/