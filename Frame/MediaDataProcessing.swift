import CoreImage
import Foundation

/// Signatures for the manipulation of Frame media data.
///
/// :Author: Ryan Wachowski
/// :Revision: 1.0
/// :Date: 03/20/15
public protocol MediaDataProcessing {
    
    /// Reverts all changes to the image.
    ///
    /// :param: callback    performs post-change logic (such as updating views)
    func reset(callback: (update: CIImage) -> Void)
    
    
    
}